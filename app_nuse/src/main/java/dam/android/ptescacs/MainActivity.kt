package dam.android.ptescacs

import android.content.ClipData
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.view.DragEvent
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.ColorInt
import androidx.annotation.RequiresApi
import androidx.core.view.children
import androidx.core.view.isEmpty
import androidx.core.view.isNotEmpty
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.collections.ArrayList
import kotlin.collections.indexOf as indexOf1

class MainActivity : AppCompatActivity() {


    lateinit var LLinicial: LinearLayout
    var cotadorTiradas : Int = 0

    companion object {
        var cambio: ArrayList<Coordenada>? = null

        var arrayCoordenadas: ArrayList<ArrayList<LinearLayout>> = arrayListOf()
        var image: ImageView? = null
        var btCambiarPieza: Button? = null
        var modoFicha : Int = 0;

        private fun cargarCoordenadas(): Coordenada {

            var posicion :Coordenada = Coordenada(0,0)

            for (i in 0..7) {
                if (arrayCoordenadas.get(i).indexOf(image!!.parent as LinearLayout) != -1) {
                    posicion = Coordenada(i,arrayCoordenadas.get(i).indexOf(image!!.parent as LinearLayout))
                }
            }
            return posicion
        }
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var chronometer: Chronometer = findViewById(R.id.chronometer)
        var tiradas : TextView = findViewById(R.id.tiradas)
        var tablero = findViewById<TableLayout>(R.id.tablero)

        chronometer.start()

        btCambiarPieza = findViewById(R.id.cambiarPieza) as Button

        image = ImageView(this)
        image!!.setImageResource(R.drawable.knight)
        image!!.setTag(R.drawable.knight, "knight")

        image!!.layoutParams = LinearLayout.LayoutParams(125, 125)

        for (i in 0..7) {
            val linea = TableRow(this)
            val array3: ArrayList<LinearLayout> = arrayListOf()

            for (k in 0..7) {
                var aux = LinearLayout(this)

                aux.minimumWidth = 130
                aux.minimumHeight = 130

                if ((i + k) % 2 == 0) {
                    aux.setBackgroundColor(Color.BLACK)
                } else {
                    aux.setBackgroundColor(Color.WHITE)
                }

                if (i == 5 && k == 5) {
                    aux.addView(image)
                    LLinicial = aux
                }

                aux.setOnDragListener(ContainerDragListener())
                linea.addView(aux)
                array3.add(aux)
            }
            arrayCoordenadas.add(array3)
            tablero.addView(linea)
        }
        image!!.setOnTouchListener(imgTouchListener())
        cargarCoordenadas()
        you(image!!);
    }

    private class imgTouchListener : View.OnTouchListener {
        override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {

            if (motionEvent.action == MotionEvent.ACTION_DOWN) {

                val shadowBuilder = View.DragShadowBuilder(view)
                view.startDrag(null, shadowBuilder, view, 0)

                view.setVisibility(View.VISIBLE)

                when (view.tag.toString()) {
                    "knight" -> {
                        cambio = calcularMovimientosCaballo(cargarCoordenadas())
                    }
                    "alfil" -> {
                        cambio = calcularMovimientosAlfil(cargarCoordenadas())
                    }
                    "torre" -> {
                        cambio = calcularMovimientosTorre(cargarCoordenadas())
                    }
                    "peon" -> {
                        cambio = calcularMovimientosPeon(cargarCoordenadas())
                    }
                }

              cambio!!.forEach({v -> arrayCoordenadas.get(v.fila).get(v.columna).setBackgroundColor(Color.YELLOW)})

                return true
            } else {
                return false
            }
        }

        private fun calcularMovimientosPeon(co : Coordenada) : ArrayList<Coordenada> {

            val arrayPosValidas: ArrayList<Coordenada> = arrayListOf()
            val columna = co.columna
            val fila = co.fila

            if((fila -1 <= 7)&& (fila - 1 >= 0)) {

                var cor = Coordenada(fila-1,columna)
                arrayPosValidas.add(cor)
            }
            return arrayPosValidas
        }

        private fun calcularMovimientosAlfil(co : Coordenada) : ArrayList<Coordenada> {

            val arrayPosValidas: ArrayList<Coordenada> = arrayListOf()
            val columna = co.columna
            val fila = co.fila
            var i = fila-1
            var k = columna+1

            while(i >= 0 && k <= 7) {
                if((i >= 0)&&( k <= 7)) {
                    var cor = Coordenada(i, k)
                    arrayPosValidas.add(cor)
                }
                i--
                k++
            }

            i = fila+1
            k = columna+1

            while(i <= 7 && k <= 7) {
                if((i <= 7)&&(k <= 7)) {
                    var cor = Coordenada(i, k)
                    arrayPosValidas.add(cor)
                }
                i++
                k++
            }

            i = fila+1
            k = columna-1

            while(i <= 7 && k >= 0) {
                if((i <= 7)&&(k >= 0)) {
                    var cor = Coordenada(i, k)
                    arrayPosValidas.add(cor)
                }
                i++
                k--
            }

            i = fila-1
            k = columna-1

            while(i >= 0 && k >= 0) {
                if((i >= 0)&&(k >= 0)) {
                    var cor = Coordenada(i, k)
                    arrayPosValidas.add(cor)
                }
                i--
                k--
            }
            return arrayPosValidas
        }

        private fun calcularMovimientosTorre(co : Coordenada) : ArrayList<Coordenada> {

            val arrayPosValidas: ArrayList<Coordenada> = arrayListOf()
            val columna = co.columna
            val fila = co.fila

            for( i in fila-1 downTo 0) {

                var cor = Coordenada(i, columna)
                arrayPosValidas.add(cor)
            }

            for( i in fila+1 .. 7) {

                var cor = Coordenada(i, columna)
                arrayPosValidas.add(cor)
            }

            for( i in columna-1 downTo 0) {

                var cor = Coordenada(fila, i)
                arrayPosValidas.add(cor)
            }

            for( i in columna+1 .. 7) {

                var cor = Coordenada(fila, i)
                arrayPosValidas.add(cor)
            }
            return arrayPosValidas
        }

        private fun calcularMovimientosCaballo(co : Coordenada) : ArrayList<Coordenada> {

            val arrayPosValidas: ArrayList<Coordenada> = arrayListOf()
            val columna = co.columna
            val fila = co.fila

            if((columna-2 >= 0) && (fila -1 >= 0)) {

                var cor = Coordenada(fila-1, columna-2)
                arrayPosValidas.add(cor)
            }

            if((columna-2 >= 0) && (fila +1 <= 7)) {

                var cor = Coordenada(fila+1, columna-2)
                arrayPosValidas.add(cor)
            }

            if((columna+2 <= 7) && (fila -1 >= 0 )) {

                var cor = Coordenada(fila-1, columna+2)
                arrayPosValidas.add(cor)
            }

            if((columna+2 <= 7) && (fila +1 <= 7 )) {

                var cor = Coordenada(fila+1, columna+2)
                arrayPosValidas.add(cor)
            }

            if((columna-1 >= 0) && (fila + 2 <= 7 )) {
                var cor = Coordenada(fila+2, columna-1)
                arrayPosValidas.add(cor)
            }

            if((columna+1 <= 7) && (fila - 2 >= 0) ) {
                var cor = Coordenada(fila-2, columna+1)
                arrayPosValidas.add(cor)
            }

            if((columna+1 <= 7) && (fila + 2 <= 7) ) {

                var cor = Coordenada(fila+2, columna+1)
                arrayPosValidas.add(cor)
            }

            if((columna-1 >= 0) && (fila - 2 >= 0)) {

                var cor = Coordenada(fila-2, columna-1)
                arrayPosValidas.add(cor)
            }

            return arrayPosValidas
        }
    }

    private inner class ContainerDragListener : View.OnDragListener {

        override fun onDrag(v: View, event: DragEvent): Boolean {

            val action = event.action
            var color = v.background

            when (action) {

                DragEvent.ACTION_DRAG_STARTED -> {

                }

                DragEvent.ACTION_DRAG_ENTERED -> {

                }

                DragEvent.ACTION_DRAG_EXITED -> {

                }

                DragEvent.ACTION_DROP -> {

                    val view = event.localState as View

                    if( ((v as LinearLayout).background as ColorDrawable).color == Color.YELLOW ) {

                        val oldContainer = view.getParent() as LinearLayout
                        oldContainer.removeView(view)
                        val newContainer = v as LinearLayout
                        newContainer.addView(view)

                        cotadorTiradas++
                        tiradas.setText(cotadorTiradas.toString())

                    }
                    view.visibility = View.VISIBLE

                    for (i in 0..cambio!!.size-1){

                        var fila: Int =  cambio!!.get(i).fila
                        var columna: Int =  cambio!!.get(i).columna

                        if ((fila + columna) % 2 == 0) {
                            arrayCoordenadas.get(fila).get(columna).setBackgroundColor(Color.BLACK)
                        } else {
                            arrayCoordenadas.get(fila).get(columna).setBackgroundColor(Color.WHITE)
                        }
                    }
                }
            }
            return true
        }
    }

    fun you(view: View) {

        when (modoFicha) {
            0 -> {
                image!!.setImageResource(R.drawable.knight)
                image!!.tag ="knight"
            }
            1 -> {
                image!!.setImageResource(R.drawable.alfil)
                image!!.tag = "alfil"
            }
            2 -> {
                image!!.setImageResource(R.drawable.torre)
                image!!.tag = "torre"
            }
            3 -> {
                image!!.setImageResource(R.drawable.peon)
                image!!.tag = "peon"
            }
        }

        if(modoFicha < 3) {
            modoFicha++;
        }else {
            modoFicha = 0
        }

    }
}



