package com.androidjairo.u3t4event;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.media.TimedMetaData;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import java.time.Clock;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener
                            , TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    private Bundle inputData;

    private TextView tvEventName;
    private RadioGroup rgPriority;
    private TextView tvDate;
    private TextView tvTime;
    private Button btAccept;
    private Button btCancel;
    private int priority = R.string.rbNormal;
    private Resources res;
    private String[] months;
    private EditText etPlace;

    private int dia ;
    private int mesAux;
    private String mes;
    private int anyo;

    private int hora;
    private int minuto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data_activity);


        //TODO Ex1.2 Cargamos el array en el onCreate
        res = getResources();
        months = res.getStringArray(R.array.months);

        setUI();

        inputData = getIntent().getExtras();
        tvEventName.setText(inputData.getString("EventName"));

        /**
         * Llamamos a el metodo que carga los valores de los widgets
         */
        saveDateTime();

    }

    private void setUI() {

        tvEventName = (TextView) findViewById(R.id.tvEventName);
        rgPriority = (RadioGroup) findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);

        tvDate = (TextView) findViewById(R.id.tvDate);
        tvTime = (TextView) findViewById(R.id.tvTime);

        btAccept = (Button) findViewById(R.id.btAccept);
        btCancel = (Button) findViewById(R.id.btCancel);

        etPlace = (EditText) findViewById(R.id.etPlace);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);

        rgPriority.setOnCheckedChangeListener(this);
    }

    /**
     * TODO Ex4 metodos para abrir los widgets al hacer click en el boton
     * @param v
     */
    public void windowDate(View v){

        DatePickerCalendar date = new DatePickerCalendar(this);
        date.show(getSupportFragmentManager(), "dateTag");
    }

    /**
     * TODO Ex4 metodos para abrir los widgets al hacer click en el boton
     * @param v
     */
    public void windowHour(View v){

        TimePickerHour time = new TimePickerHour(this);
        time.show(getSupportFragmentManager(),"timeTag");
    }

    /**
     * TODO EX4 saveDateTime
     *
     * Este metodo sirve para cargar los datos que teniamos anteriormente o los datos actuales
     * (fecha y hora del momento)
     *
     * Lo he puesto en un metodo para no tener un setUI tan largo.
     */
    private void saveDateTime(){
        if(inputData.size() > 2) {

            dia = inputData.getInt("day");
            mesAux = inputData.getInt("month");
            mes = months[inputData.getInt("month")];
            anyo = inputData.getInt("year");

            tvDate.setText(dia + " " + mes + " " + anyo);

            hora = (int) inputData.get("hour");
            minuto = (int) inputData.get("minute");

            tvTime.setText(hora + ":" + minuto);

        } else {
            Calendar cal = Calendar.getInstance();

            dia = cal.get(Calendar.DAY_OF_MONTH);
            mes = months[cal.get(Calendar.MONTH)];
            mesAux = cal.get(Calendar.MONTH);
            anyo = cal.get(Calendar.YEAR);

            tvDate.setText(dia + " " + mes + " " + anyo);

            hora = cal.get(Calendar.HOUR_OF_DAY);
            minuto = cal.get(Calendar.MINUTE);

            tvTime.setText(hora + ":" + minuto);
        }
    }


    /**
     * TODO Ex 3 Cambio de String
     * He modificado la manera en la que le paso los datos ya que con una sola cadena,
     * los valores no pueden ser traducidos, por lo que le paso cada uno de manera
     * individual para poder cambiar cada palabra de idioma
     * @param view
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {

        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        switch(view.getId()) {
            case R.id.btAccept:

                // TODO Ex1.3 Creamos las cadenas que utilizaremos en el string.xml

                eventData.putInt("day", dia);
                eventData.putInt("month", mesAux);
                eventData.putInt("year", anyo);
                eventData.putInt("priority", priority);
                eventData.putInt("hour", hora);
                eventData.putInt("minute", minuto);
                eventData.putString("place", etPlace.getText().toString());

                activityResult.putExtras(eventData);
                setResult(RESULT_OK,activityResult);

                break;

                //TODO Ex1.1 Cargamos los valores los valores previos
            case R.id.btCancel:

                setResult(RESULT_CANCELED);
                break;
        }
        finish();
    }

    /**
     * Tambien he cambiado la manera de poner estas strings ya que si no,
     * no cambiarian de idioma
     * @param radioGroup
     * @param i
     */
    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {

        switch (i) {
            case R.id.rbLow:
                priority = R.string.rbLow;
                break;
            case R.id.rbNormal:
                priority = R.string.rbNormal;
                break;
            case R.id.rbHigh:
                priority = R.string.rbHigh;
                break;
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        hora = hourOfDay;
        minuto = minute;

        tvTime.setText(hora + ":" + minuto);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)
    {
        dia = dayOfMonth;
        mesAux = month;
        anyo = year;

        tvDate.setText(dia + " " + mes + " " + anyo);

    }
}