package com.androidjairo.u3t4event;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final int REQUEST = 0;

    private EditText etEventName;
    private TextView tvCurrentData;
    private EditText etPlace;

    /**
     * TODO Ex3 Cambiamos el bundle
     *
     * Ahora el bundle lo pongo como global para poder crear uno nuevo en el setUI
     * que se ejecutara cada vez que se cree la activity
     */
    Intent intent;
    Bundle bundle ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {

        etEventName = (EditText) findViewById(R.id.etEventName);
        tvCurrentData = (TextView) findViewById(R.id.tvCurrentData);
        etPlace = (EditText) findViewById(R.id.etPlace);

        if(bundle == null) {
            bundle = new Bundle();
        }
    }

    public void editEventData(View view) {

        intent = new Intent(this, EventDataActivity.class);

        // set info data to bundle
        bundle.putString("EventName", etEventName.getText().toString());

        //TODO Ex1.1 Obtenemos los mismos valores para recuperar los datos anteriorres
        // guardamos lo valores por si pulsamos cancelar, tener guardado el anterior, ja que no se modificara

        bundle.putString("CurrentData", tvCurrentData.getText().toString());

        // add bundle to intent
        intent.putExtras(bundle);
        //call activity and there will be result
        startActivityForResult(intent, REQUEST);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //check if result comes from EventDataActivity and finished OK
       if(requestCode == REQUEST && resultCode == RESULT_OK) {
           bundle = data.getExtras();

           /**
            * Recojo los valores por separados y los junto para formar la cadena deseada, de esta forma
            * cuando cambiamos de idioma tambien cambian los valores, ja que no tenemos una String solamente.
            */
           String[] cosas = getResources().getStringArray(R.array.months);
           String mes = cosas[data.getIntExtra("month", 0)] + " ";

            tvCurrentData.setText(getString(R.string.date, data.getIntExtra("day",0)+ " " + mes + " " +
                                  data.getIntExtra("year",0)) + "\n" +
                                  getString(R.string.priority, getString(data.getIntExtra("priority",R.string.rbNormal))) +"\n" +
                                  getString(R.string.hour, data.getIntExtra("hour",0) + ":" +
                                                                       data.getIntExtra("minute",0)) +"\n" +
                                  getString(R.string.place, data.getStringExtra("place")));
       }
    }

    //TODO Ex1.3 Preservar el estado de los datos existentes

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        /**
         * TODO Ex3 Guardamos el Bundel envez de un texto
         *
         * Ahora ya no conservamos un text, sino un bundle que
         * en caso de que tengamos datos en el bundle, lo rellenaremos en
         * el onRestore.
         */
        savedInstanceState.putBundle("bundle", bundle);

    }

    /**
     * Tambien he modificado el restore para poder cambiar de idioma
     * @param savedInstanceState
     */
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        bundle = savedInstanceState.getBundle("bundle");

        /**
         * Si el bundle es la primera vez que se crea, no deberemos cargar los datos ya que
         * las cadenas estaran vacias (No hemos guardado nada todavia.)
         */
        if(bundle.size() > 0) {

            String[] cosas = getResources().getStringArray(R.array.months);
            String mes = cosas[bundle.getInt("month",0)] + " ";

            tvCurrentData.setText(getString(R.string.date, bundle.getInt("day",0)+ " " + mes + " " +
                    bundle.getInt("year",0)) + "\n" +
                    getString(R.string.priority, getString(bundle.getInt("priority",R.string.rbNormal))) +"\n" +
                    getString(R.string.hour, bundle.getInt("hour",0) + ":" +
                            bundle.getInt("minute",0)) +"\n" +
                    getString(R.string.place, bundle.getString("place")));
        }
    }
}
