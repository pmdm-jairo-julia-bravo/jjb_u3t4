package com.androidjairo.u3t4event;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final int REQUEST = 0;

    private EditText etEventName;
    private TextView tvCurrentData;
    private EditText etPlace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {

        etEventName = (EditText) findViewById(R.id.etEventName);
        tvCurrentData = (TextView) findViewById(R.id.tvCurrentData);
        etPlace = (EditText) findViewById(R.id.etPlace);

    }

    public void editEventData(View view) {

        Intent intent = new Intent(this, EventDataActivity.class);
        Bundle bundle = new Bundle();

        // set info data to bundle
        bundle.putString("EventName", etEventName.getText().toString());

        //TODO Ex1.1 Obtenemos los mismos valores para recuperar los datos anteriores
        // guardamos lo valores por si pulsamos cancelar, tener guardado el anterior, ja que no se modificara

        bundle.putString("CurrentData", tvCurrentData.getText().toString());

//        bundle.putString("PlaceName", etPlace.getText().toString());
        // add bundle to intent
        intent.putExtras(bundle);
        //call activity and there will be result
        startActivityForResult(intent, REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //check if result comes from EventDataActivity and finished OK
        if(requestCode == REQUEST && resultCode == RESULT_OK) {
            tvCurrentData.setText(data.getStringExtra("EventData"));
        }
    }

    //TODO Ex1.3 Preservar el estado de los datos existentes

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putString("CurrentData", tvCurrentData.getText().toString());
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        tvCurrentData.setText(savedInstanceState.getString("CurrentData"));
    }
}
