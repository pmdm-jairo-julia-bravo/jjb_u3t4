package com.androidjairo.u3t4event;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.media.TimedMetaData;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;
import static android.content.res.Configuration.ORIENTATION_PORTRAIT;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener{

    private Bundle inputData;

    private TextView tvEventName;
    private RadioGroup rgPriority;
    private DatePicker dpDate;
    private TimePicker tpTime;
    private Button btAccept;
    private Button btCancel;
    private String priority = "Normal";
    private Resources res;
    private String[] months;
    private EditText etPlace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data_activity);

        setUI();

        inputData = getIntent().getExtras();

        tvEventName.setText(inputData.getString("EventName"));

        //TODO Ex1.2 Cargamos el array en el onCreate
        res = getResources();
        months = res.getStringArray(R.array.months);
    }

    private void setUI() {

        tvEventName = (TextView) findViewById(R.id.tvEventName);
        rgPriority = (RadioGroup) findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);

        dpDate = (DatePicker) findViewById(R.id.dpDate);

        tpTime = (TimePicker) findViewById(R.id.tpTime);
        tpTime.setIs24HourView(true);
        btAccept = (Button) findViewById(R.id.btAccept);
        btCancel = (Button) findViewById(R.id.btCancel);

        //TODO Ejercicio 1.3 creacion del editText de place
        etPlace = (EditText) findViewById(R.id.etPlace);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);

        rgPriority.setOnCheckedChangeListener(this);

        //TODO Ejercicio 2 Solo el spiner en vertical

        /**
         * Obtenemos el los recursos y dependiendo de la orientacion del dispositivo,
         * en vertical solo se mostrara en vertical, pero en horizontal se mostrara
         * el soiner y el calendario.
         */
        res = getResources();
        if(res.getConfiguration().orientation == ORIENTATION_LANDSCAPE) {
            dpDate.setCalendarViewShown(true);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {

        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        switch(view.getId()) {
            case R.id.btAccept:

                // TODO Ex1.3 Creamos las cadenas que utilizaremos en el string.xml
                String date = getString(R.string.date, dpDate.getDayOfMonth() + " " + months[dpDate.getMonth()] + " " + dpDate.getYear());
                String prio = getString(R.string.priority,  priority);
                String hour = getString(R.string.hour, tpTime.getHour() + ":" + tpTime.getMinute());
                String place = getString(R.string.place, etPlace.getText());

                /* Pasamos lo valores guardados en las variables anteriores */
                eventData.putString("EventData", place  +
                                          "\n" + prio +
                                          "\n" + date +
                                          "\n" + hour);

                //eventData.putString("PlaceName", inputData.getString("PlaceName"));
                break;

                //TODO Ex1.1 Cargamos los valores los valores previos
            case R.id.btCancel:

                /*En caso de pulsar el boton de cancelar, cargamos los datos anteriores*/
                eventData.putString("EventData", inputData.getString("CurrentData"));
                break;
        }
        activityResult.putExtras(eventData);
        setResult(RESULT_OK,activityResult);

        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {

        switch (i) {
            case R.id.rbLow:
                priority = "Low";
                break;
            case R.id.rbNormal:
                priority = "Normal";
                break;
            case R.id.rbHigh:
                priority = "High";
                break;

        }
    }
}